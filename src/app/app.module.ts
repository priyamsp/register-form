import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, APP_INITIALIZER } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { AppRoutingModule } from './app-routing.module';
import { NgOtpInputModule } from 'ng-otp-input';
import { I18nCountrySelectModule, I18nCountrySelectService } from 'ngx-i18n-country-select';
export function setUpI18nCountrySelect(service: I18nCountrySelectService) {
  return () => service.use(['de', 'en']);
}
@NgModule({

  declarations: [

    AppComponent


  ],

  imports: [

    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2TelInputModule,
    AppRoutingModule,
    NgOtpInputModule,
    I18nCountrySelectModule.forRoot()


  ],

  providers: [
    { provide: LOCALE_ID, useValue: 'de-DE' },
    I18nCountrySelectService,
    {
      provide: APP_INITIALIZER,
      useFactory: setUpI18nCountrySelect,
      deps: [I18nCountrySelectService],
      multi: true
    }
  ],

  bootstrap: [AppComponent]

})

export class AppModule { }