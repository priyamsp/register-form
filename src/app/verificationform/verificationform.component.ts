import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-verificationform',
  templateUrl: './verificationform.component.html',
  styleUrls: ['./verificationform.component.css']
})
export class VerificationformComponent implements OnInit {

  constructor() { }
  form3 = new FormGroup({
    one: new FormControl('', [Validators.required, Validators.minLength(1)]),

  });



  get f() {

    return this.form3.controls;

  }



  submit() {

    console.log(this.form3.value);
    localStorage.setItem('form-data', JSON.stringify(this.form3.value));


  }
  ngOnInit(): void {
  }

}
