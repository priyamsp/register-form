import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationformComponent } from './verificationform.component';

describe('VerificationformComponent', () => {
  let component: VerificationformComponent;
  let fixture: ComponentFixture<VerificationformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificationformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
