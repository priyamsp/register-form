import { Component, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
//import { AnyARecord } from 'dns';



@Component({

  selector: 'app-root',

  templateUrl: './app.component.html',

  styleUrls: ['./app.component.css']

})

export class AppComponent {


  item = {
    isocode: 'de'
  };
  isDiv1: boolean = true;
  isDiv2: boolean = false;
  isDiv3: boolean = false;
  isDiv4: boolean = false;


  otp: string;
  showOtpComponent = true;
  @ViewChild('ngOtpInput', { static: false }) ngOtpInput: any;
  config = {
    allowNumbersOnly: false,
    length: 5,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: '',
    inputStyles: {
      'width': '77px',
      'height': '60px'
    }

  };


  counryList: any = ['India', 'USA']
  stateList: any = ['Tamilnadu', 'Assam', 'Odisa', 'Karnataka', 'kerala']

  form = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    gender: new FormControl('', Validators.required),
    // country: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required),
    code: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]),
    companyname: new FormControl('', [Validators.required, Validators.minLength(3)]),
    email: new FormControl('', [Validators.required, Validators.email, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
    job: new FormControl('', Validators.required),
    experience: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$")]),
    checkbox: new FormControl('', Validators.required),
    logo: new FormControl('', Validators.required)




  });

  get f() {

    return this.form.controls;

  }
  onOtpChange(otp) {
    this.otp = otp;
  }

  submit() {

    console.log(this.form.value);
    localStorage.setItem('form-data', JSON.stringify(this.form.value));

  }

  toggleShow() {
    this.isDiv1 = !this.isDiv1;
    this.isDiv2 = !this.isDiv2;
    this.isDiv3 = this.isDiv3;

  }
  toggleShow2() {
    this.isDiv1 = !this.isDiv1;
    this.isDiv2 = !this.isDiv2;
    this.isDiv3 = !this.isDiv3;

  }
  toggleShow3() {
    this.isDiv1 = !this.isDiv1;
    this.isDiv2 = !this.isDiv2;
    this.isDiv3 = !this.isDiv3;

  }


}