import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyformComponent } from './companyform/companyform.component';
import { VerificationformComponent } from './verificationform/verificationform.component';


const routes: Routes = [
  {
    path: 'companyform', component: CompanyformComponent
  },
  {
    path: 'verificationform', component: VerificationformComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
