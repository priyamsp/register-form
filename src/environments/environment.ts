// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyD8whA_hq8TwNEWis3K9VD7OuDxFfJCDKo",
    authDomain: "resigtration.firebaseapp.com",
    databaseURL: "https://resigtration.firebaseio.com",
    projectId: "resigtration",
    storageBucket: "resigtration.appspot.com",
    messagingSenderId: "682762589440",
    appId: "1:682762589440:web:1c85c7a565d2eed0c54fe5",
    measurementId: "G-1H19PF0SWM"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
